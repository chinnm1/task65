package com.task65s50.restapi.respository;

import org.springframework.data.repository.CrudRepository;

import com.task65s50.restapi.model.CUser;

public interface ICuserRespository extends CrudRepository<CUser, Long> {

}
