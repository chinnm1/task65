package com.task65s50.restapi.controller;

import java.lang.StackWalker.Option;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task65s50.restapi.model.COrder;
import com.task65s50.restapi.respository.ICorderRespository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/v1/order")
public class COrderController {
    @Autowired
    ICorderRespository orderRespository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllOrders() {
        List<COrder> orderList = new ArrayList<COrder>();
        try {
            orderRespository.findAll().forEach(orderElement -> {
                orderList.add(orderElement);
            });
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(name = "id") Long id) {
        Optional<COrder> _order = orderRespository.findById(id);
        if (_order.isPresent()) {
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder newOrde) {
        COrder _order = new COrder();
        try {
            _order.setCreated(new Date());
            _order.setUpdated(null);
            _order.setOrderCode(newOrde.getOrderCode());
            _order.setVoucherCode(newOrde.getVoucherCode());
            _order.setPizzaSize(newOrde.getPizzaSize());
            _order.setPizzaType(newOrde.getPizzaType());
            _order.setPrice(newOrde.getPrice());
            _order.setPaid(newOrde.getPaid());
            _order.setcUser(null);

            orderRespository.save(_order);
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation about this entity" + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> _orderData = orderRespository.findById(id);
        if (_orderData.isPresent()) {
            COrder _order = _orderData.get();
            _order.setUpdated(new Date());
            _order.setPizzaSize(orderUpdate.getPizzaSize());
            _order.setPizzaType(orderUpdate.getPizzaType());
            _order.setPrice(orderUpdate.getPrice());
            _order.setPaid(orderUpdate.getPaid());
            _order.setVoucherCode(orderUpdate.getVoucherCode());
            try {
                return ResponseEntity.ok(orderRespository.save(_order));

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation about this entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable("id") Long id) {
        Optional<COrder> _orderData = orderRespository.findById(id);
        if (_orderData.isPresent()) {
            try {
                orderRespository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());

            }
        } else {
            return new ResponseEntity<Object>("User not found", HttpStatus.NOT_FOUND);
        }
    }

}
