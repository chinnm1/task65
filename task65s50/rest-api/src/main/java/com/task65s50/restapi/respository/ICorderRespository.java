package com.task65s50.restapi.respository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.task65s50.restapi.model.COrder;

public interface ICorderRespository extends CrudRepository<COrder, Long> {
    // List<COrder> findByUserId(Long id);

}
